**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

twknrzzLADUnxWiVKn3c78nYcKr87pAJw8JjO8OQpEQGUwibRUObC79baZasd4xKJU3dZp4tdDh23G2yWMlvbQ98GCNheMkiAj40S76l5AefkfG5BTQ9SulbwrfCQpcxlQwbwyZNTKhSvDpA0gBSnFOXbddMpSALAJlqQbemjXK9Q8xAAUv7cG1VDUhGBybsvQeem2uPJ0k4qXCVoqRuG9mXWMr3cfAPFRuBXMez2h8zLrhWOStIWTUrY4JZ935XitYbewRS1MDe2yXOxfVDPk1u4UaBgJlNdHWAVQCy8xjz8NcvA5r5wxojWgbS8oBOliQ1YSdHCmGMRJ1Hy6x2PZ9WXRWBMgTDFg8HK3Tnwu7DVoiHGBM1YxYtMxjRmQbFEcLu8wAorlTm0lFvVxdH4Z4vsbgeWj9iHFQ7ZVtQYZ3d1be0DWvwVQwhYbhGNhbCf78DtGHuMMy5VOVAsRKmVhYKahUMCMdBKm7fHn8NNPMrq7eRF9XTRI6c2WDewCfRajtB5o61Z74IMMjQ8LuuZWbhvaD9tNMhoFudL2brAKsPxjK0ud2R2lfnXxgWLdOCm2ENZ0geOtjJvBT6zCl78wM72rtpSw0pPIJ25c+obARdGaCrIOwuAzmzN/H6VMWH59qs8Tg4Fw4DxSRXeRosYDR6t8vKXI/hdQ1ZwxdhkNHqBK+IqjlPpYZhDAY4OrNimYFoug8+fnvhd5FbqPC4NIrHwgvvQPLXLtWSahnsfJ08iZ96Ob+nKTxC+4q5sr4kLeABk9MFPMPUMNahu17Yt31cUyAnCLOZPY9yJj3yoC+gWYtu/XX6+1YoIH8F+Cwg1nbUZkGbbzDkOhXwHgwo9v6ThIKPfINgSole2JKDh88tE0dOjFob1EpOBiD8sZh3PvYI/EFe8NieezNoHSBUEovdioo+I+I9jb0qEZfz+h5zHjxz7LDAH7DWIJKjBMqz3ulZl3E5+EkMt6AyDAa8teFqtyciO2PfZMR68eEx43ESUFkd/IykkksvrrnZhPnr6u9n7uDC51Y3G4AyNlGtvj/by8M7xkDaKwWiUxmNNLXlbZcd0W9jJywbc0kzJKYUSIbZUdsjAEuqthnqtHPz/AHX5FsW5+LpqfhDmvOKPFmBNl4Fqf4rjzM0iNWZTkzpP8gdHPtdmBOs037ZMHpKDA==zeIIMXj3pmlVejVQnL0EhsCPmTUHw7SeMRKiQiSIpsTqmWKtWJOd8kRMdJWNsmNPqthCO666CPUn7QtLiTaA6obH6aM5PosQ56lluHoVhIMStGpbFBmCuwva8DFlqI5bvKhhUuv8wRtibv2oOsVSOjLxyZYqcxmOswmJgbQLE520P4DfVbM5Naof1j5NG4Uz36kUWR3HECrQZ71ZdWjswGxFR50KYEqr09Vkg8XFCkECdXlWI8kOd99tzk1JuxE5hwoGJSKSfUeKEc6pG2ZZpNlAQJRXrnJj93a9SYAlRRtv3RY3jKPEsHX0r5zRhDuOI84JeilWsydPtY3mNlgpxWG28onRY18JsywYtWFKvandUxf0xHhgk7VvFuxolWXKOfVyh2ddgP13VuGp9qtmStfZAeF1tXFnyFRxLU6SxfBpUJ6EMJ1Jmvw8YRiseEUUabry8s6bCAci1We0VZ6lMJlA8sHYyZaWXInJaLppKHgGxguXQjUgTN26JpByPK0NIuW1SzxTjcrQmWHLhoumc42wuEvhrgBz7PpalkM15qZMY9UNUOLlbGtothUDn8Kt
Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).